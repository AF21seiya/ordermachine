import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrderMachine {
    int sum=0;
    void order(String food,int price) {
        if (food == "Checkout"){
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to Checkout?",
                    "Checkout Confirmation",
                    JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {

                    orderdItemsList.setText("");
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is "+sum+" yen");
                    sum=0;
                    sum_price.setText("Total " +sum + " yen");
                }
            return;
        }

        String[] quantity = {"1","2","3","4","5"};//皿の枚数

        String currentText = orderdItemsList.getText();

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            int plate = 1+JOptionPane.showOptionDialog(
                                null,
                                "How many plates would you like to order?",
                                "How many plates?",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE,
                                null, quantity,
                                null
                                //０番目の要素に１が対応しているので１を押したときにplateに０が代入されてしまう。１を代入したいので１を足す
                        );

            orderdItemsList.setText(currentText + plate+" plates of "+ food + " " + (price*plate + (price*plate)/10) + " yen"+"\n");
            JOptionPane.showMessageDialog(null, "Order for " + plate + " plates of " + food + " received.");
            sum += price*plate + (price*plate)/10;//消費税１０％として計算
            sum_price.setText("Total " +sum + " yen");
        }
    }

    private JButton tunaButton;
    private JButton salmonButton;
    private JButton tekkamakiButton;
    private JButton cakeButton;
    private JButton orangeJuiceButton;
    private JButton ramenButton;
    private JTextPane orderdItemsList;
    private JButton checkOutButton;
    private JPanel root;
    private JTextPane sum_price;

    public OrderMachine() {

    tunaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Tuna",200);
        }
    });
    tunaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tuna.png")
        ));
    salmonButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Salmon",120);
        }
    });
    salmonButton.setIcon(new ImageIcon(
                this.getClass().getResource("Salmon.jpg")
    ));
    tekkamakiButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Tekkamaki",100);
        }
    });
    tekkamakiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tekkamaki.jpg")
    ));
    ramenButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("ramen",400);
        }
    });
    ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.jpg")
    ));
    orangeJuiceButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Orange juice",150);
        }
    });
    orangeJuiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("Orange.jpg")
    ));
    cakeButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("cake",250);
        }
    });
    cakeButton.setIcon(new ImageIcon(
                this.getClass().getResource("Cake.jpg")
    ));

    checkOutButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order ("Checkout",0);
        }
    });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("OrderMachine");
        frame.setContentPane(new OrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
